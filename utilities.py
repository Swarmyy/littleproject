from inspect import signature


def group(l, callbackFn):
    res = {}
    for i, m in enumerate(l):
        key = {
            1: lambda: callbackFn(m),
            2: lambda: callbackFn(m, i),
            3: lambda: callbackFn(m, i, l)
        }[len(signature(callbackFn).parameters)]()
        try:
            res[key].append(m)
        except KeyError:
            res[key] = [m]
    return res


if __name__ == "__main__":
    inventory = [
        {"name": 'asparagus', "type": 'vegetables', "quantity": 5},
        {"name": 'bananas', "type": 'fruit', "quantity": 0},
        {"name": 'goat', "type": 'meat', "quantity": 23},
        {"name": 'cherries', "type": 'fruit', "quantity": 5},
        {"name": 'fish', "type": 'meat', "quantity": 22}
    ]

    print(group(inventory, lambda m: m["type"]))
