from random import Random


class Selector:
    def __init__(self, divs):
        self.divs = divs
        self.changed = {key: False for key in divs[0].keys()}

    def change_v(self, i, k, v):
        """
        Updates the name by the value at the index, but also all the other ones if they were not already changed
        :param i: index of the updated object
        :param k: name of value updated
        :param v: value
        """
        self.divs[i][k] = v
        for d in self.divs:
            if not self.changed[k]:
                d[k] = v
        self.changed[k] = True

    def reset_changes(self):
        for k in self.changed.keys():
            self.changed[k] = False


if __name__ == "__main__":
    # data[:index_to_remove] + data[index_to_remove + 1:]
    sdivs = [{"first": Random().randint(1, 10), "second": Random().randint(1, 10), "third": Random().randint(1, 10)} for
             _ in range(10)]
    sel = Selector(sdivs)
    print(sel.divs)
    sel.change_v(3, "third", 9)
    print(sel.divs)
    sel.change_v(3, "third", 1)
    print(sel.divs)
