from tkinter import Tk
from chiffrelettres import ChiffreLettre
from Minesweeper.Minesweeper import Minesweeper
from Minesweeper.UI.UI import UI


def init_ui(grid):
    root = Tk()
    root.title("Minesweeper")
    UI(root, grid.board)
    root.mainloop()


def print_grid(grid):
    # print(grid1.luck)
    # print(grid1)
    # print("\n")
    print(grid1.slug_board(grid1.board))


if __name__ == '__main__':
    # print(ChiffreLettre(800).__str__())
    grid1 = Minesweeper(3, 3, luck=0.2)
    print_grid(grid1)
    not_end = True
    while not_end:
        print("which coord you wanna tap?")
        coords = input("x,y: ").split(",")
        x, y = coords[0], coords[1]
        print(x, y)
        not_end = grid1.tap(int(x), int(y))
        print(grid1)
    if grid1.is_won():
        print("You won")
    else:
        print("You lost")

    # init_ui(grid1)
