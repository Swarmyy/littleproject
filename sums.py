import timeit


def sumb(n):
    return n * (n + 1) >> 1


def summ(n):
    return int(n * (n + 1) / 2)


def sumr(n):
    return n if n <= 1 else n + sumr(n - 1)


if __name__ == '__main__':
    n = 10 ** 150
    for s in [sumb, summ]:
        start = timeit.default_timer()
        s(n)
        end = timeit.default_timer()
        print(f"{s.__name__} : {(end - start) * (10 ** 7)}")
