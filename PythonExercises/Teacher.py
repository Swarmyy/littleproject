class Teacher:
    SALARY = 2000
    HOUR_THRESHOLD = 192
    ADDITIONAL = 40
    MAX_HOURS = -1

    def __init__(self, name, hours):
        self.name = name
        self.hours = hours

    def salary(self):
        return self.SALARY * 12 + (max(self.hours, self.MAX_HOURS) - self.HOUR_THRESHOLD) * self.ADDITIONAL

    def salary_taxes(self):
        return self.salary() * 0.9


class Contractor(Teacher):
    SALARY = 0
    HOUR_THRESHOLD = 0

    def __init__(self, name, hours, organisation):
        super().__init__(name, hours)
        self.organisation = organisation


class Doctoral(Teacher):
    SALARY = 0
    HOUR_THRESHOLD = 0
    ADDITIONAL = 30
    MAX_HOURS = 96

    def __init__(self, name, hours):
        super().__init__(name, hours)
