def logged(func):
    def wrapper(*args, **kwargs):
        print(f"Calling {func.__name__}({args}, {kwargs})")
        ret = func(*args, **kwargs)
        print(f"Returns {ret}")
        return ret

    return wrapper


def memoize(func):
    func.cache = {}

    def wrapper(n):
        try:
            ans = func.cache[n]
        except KeyError:
            ans = func.cache[n] = func(n)
        return ans

    return wrapper


@logged
def func(test):
    return 3 * test


@memoize
def fibo(n):
    assert n >= 0
    if n < 2:
        return n
    else:
        return fibo(n - 1) + fibo(n - 2)


if __name__ == "__main__":
    # print(func(3))
    print(fibo(1))
