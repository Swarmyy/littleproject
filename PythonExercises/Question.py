class Question:
    def __init__(self, statement, answers=None, difficulty=50):
        """

        :param statement:
        :param answers: list[(correct:bool, statement:str)]
        :param difficulty:
        """
        if answers is None:
            answers = []
        self.statement = statement
        self.difficulty = difficulty
        self.answers = answers

    def answer(self, index):
        return self.answers[index][0]
