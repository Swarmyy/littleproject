class Robot:
    NORTH = "North"
    EAST = "East"
    WEST = "West"
    SOUTH = "South"

    def __init__(self, name, x=0, y=0, direction=NORTH):
        self.name = name
        self.x = x
        self.y = y
        self.direction = direction

    def advance(self):
        self._advance_steps(1)

    def _advance_steps(self, steps):
        if self.direction == self.NORTH:
            self.y += steps
        elif self.direction == self.EAST:
            self.x += steps
        elif self.direction == self.SOUTH:
            self.y -= steps
        elif self.direction == self.WEST:
            self.x -= steps

    def right(self):
        self.direction = {self.NORTH: self.EAST, self.EAST: self.SOUTH, self.SOUTH: self.WEST, self.WEST: self.NORTH}[
            self.direction]

    def __repr__(self):
        return f"<{self.name}__{self.x}_{self.y}_{self.direction}>"


class RobotNG(Robot):
    TURBO = 3

    def __init__(self, name, x=0, y=0, direction=Robot.NORTH):
        super().__init__(name, x, y, direction)
        self.is_turbo = False

    def advance(self):
        if self.is_turbo:
            self.advance_fast(self.TURBO)
        else:
            super().advance()

    def advance_fast(self, steps):
        if self.is_turbo:
            steps *= self.TURBO
        self._advance_steps(steps)

    def left(self):
        self.direction = {self.NORTH: self.WEST, self.EAST: self.NORTH, self.SOUTH: self.EAST, self.WEST: self.SOUTH}[
            self.direction]

    def half_turn(self):
        self.direction = {self.NORTH: self.SOUTH, self.EAST: self.WEST, self.SOUTH: self.NORTH, self.WEST: self.EAST}[
            self.direction]

    def __repr__(self):
        return f"<{self.name}__{self.x}_{self.y}_{self.direction}_turbo:{self.is_turbo}>"
