class Disease:
    STROKE = 'Stroke'
    DIABETES = 'Diabetes'
    ALZHEIMER = 'Alzheimer'
    TUBERCULOSIS = 'Tuberculosis'
    CIRRHOSIS = 'Cirrhosis'


class Diagnostic:
    def __init__(self, caregiver, date, disease, validity):
        """

        :param caregiver:
        :param date:
        :param disease:
        :param validity: diagnosis reliability, between 0 and 100%
        """
        self.caregiver = caregiver
        self.date = date
        self.disease = disease
        self.validity = validity


class Treatment:
    def __init__(self, staff, date, improvement, medication=None):
        """

        :param staff:
        :param date:
        :param improvement: in %
        """
        self.staff = staff
        self.date = date
        self.improvement = improvement
        self.medication = medication


class Medication:
    def __init__(self, dosage, frequency):
        self.dosage = dosage
        self.frequency = frequency


class Patient:
    def __init__(self, name, age, civility, condition=0.0, med_acts=None):
        if med_acts is None:
            med_acts = []
        self.name = name
        self.age = age
        self.civility = civility
        self.med_acts = med_acts
        self.condition = condition  # in %

    def number_of_meds(self, medication, date):
        count = 0
        for meds in self.med_acts:
            if meds is Treatment and meds.medication is not None:
                if meds.date == date and meds.medication == medication:
                    count += 1
        return count

    def take_treatment(self, treatment):
        if self.number_of_meds(treatment.medication, treatment.date) > treatment.medication.frequency:
            return False  # raise Exception("Patient already took this medication enough times")
        self.med_acts.append(treatment)
        self.condition += treatment.improvement
        return True

    def take_diagnosis(self, diagnosis):
        self.med_acts.append(diagnosis)
        return True
