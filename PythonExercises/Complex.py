class Complex:
    def __init__(self, re, img):
        self.re = re
        self.img = img

    def module(self):
        return (self.re ** 2 + self.img ** 2) ** 0.5

    def conjugate(self):
        return Complex(self.re, -self.img)

    def __add__(self, other):
        return Complex(self.re + other.re, self.img + other.img)

    def __sub__(self, other):
        return Complex(self.re - other.re, self.img - other.img)

    def __mul__(self, other):
        return Complex(self.re * other.re - self.img * other.img, self.re * other.img + self.img * other.re)

    def __repr__(self):
        # I choose to display +- because it's more readable
        return f"{self.re} + {self.img}i"
