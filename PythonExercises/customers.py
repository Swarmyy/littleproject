from random import Random

if __name__ == "__main__":
    customers = [
        {'t': f"6{Random().choice(['5', '6'])}{Random().randint(100000, 999999)}",
         'c': Random().randint(0, 300)}
        for i in range(20)]
    print(customers)
    avg_con = sum([c['c'] for c in customers]) / len(customers)
    for c in customers:
        if c['c'] > avg_con:
            print(f"Bonus for {c['t']} is: {20 * sum([int(m) for m in c['t'][:-6]])}$")
