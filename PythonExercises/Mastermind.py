from random import Random

if __name__ == "__main__":
    target = [str(Random().randint(1, 9)) for i in range(8)]
    tries = 8
    while tries > 0:
        print(f"You have {tries} tries left")
        guess = input(f"Guess the number({len(target)} digits)\n")
        if guess == ''.join(target):
            break
        disp = ""
        for i, g in enumerate(guess):
            if i > len(target) - 1:
                break
            if g == target[i]:
                disp += g
                continue
            if g in target:
                disp += "-"
                continue
            disp += "*"
        print(disp)
        tries -= 1
    if tries == 0:
        print("You lose")
    else:
        print("You win")
