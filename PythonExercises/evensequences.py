from random import Random


def air_ticket(ticket: str):
    letter_value = str(ord(ticket[0]) - ord('A') + 1)
    digit_value = sum([int(m) for m in letter_value + ticket[1:]])
    return digit_value % 9 == 8


def shift_letters(txt: str):
    # shift = len(txt.strip()) % 26 # number of chars
    shift = len(txt.split(' ')) % 26  # number of words
    res = ""
    for c in txt:
        if c.isalpha():
            start = ord('a') if c.islower() else ord('A')
            res += chr(start + ((ord(c) + shift - start) % 26))
        else:
            res += c
    return res


def even_sequences(seq: list, start_index=0):
    loc_seq = seq[start_index:]
    even_seq = []
    i = 0
    while i < len(loc_seq):
        m = loc_seq[i]
        curr_seq = []
        if i + 1 < len(loc_seq) and m % 2 == 0 and loc_seq[i + 1] % 2 == 0:
            curr_seq.append(m)
            curr_seq.append(loc_seq[i + 1])
            while i + 2 < len(loc_seq) and loc_seq[i + 2] % 2 == 0:
                curr_seq.append(loc_seq[i + 2])
                i += 1
            even_seq.append(curr_seq)
        i += 1
    return even_seq


if __name__ == "__main__":
    low = 3
    high = 20
    n = 20
    l1 = [Random().randint(low, high) for i in range(n)]
    # print(l1)
    # print(even_sequences(l1))
    # print(shift_letters("The Second Project Of Level One"))
    print(air_ticket("U19586900462"))
    print(air_ticket("U73985019381"))
