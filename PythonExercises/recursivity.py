from math import *


def list_sum(tl: list):
    if len(tl) == 0:
        return 0
    if len(tl) == 1:
        return tl[0]
    return tl[-1] + list_sum(tl[:-1])


def int_to_base(n, base):
    if base == 10:
        return n
    else:
        pass


def list_r_sum(rl: list):
    if not isinstance(rl, list):
        return rl
    if len(rl) == 0:
        return 0
    if len(rl) == 1:
        if isinstance(rl[0], list):
            return list_r_sum(rl[0])
        return rl[0]
    return list_r_sum(rl[-1]) + list_r_sum(rl[:-1])


def fact(n):
    if n == 0:
        return 1
    return n * fact(n - 1)


def fibo(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return fibo(n - 1) + fibo(n - 2)


def digit_sum(n):
    if n % 10 == n:
        return n
    return n % 10 + digit_sum(n // 10)


def sum_n_minus_even(n):
    if n <= 0:
        return 0
    return n + sum_n_minus_even(n - 2)


def harmonic_sum(n):
    if n == 1:
        return 1
    return 1 / n + harmonic_sum(n - 1)


def geometric_sum(n):
    if n < 0:
        return 0
    else:
        return 1 / (pow(2, n)) + geometric_sum(n - 1)


def r_power(a, b):
    if a == 0 and b == 0:
        raise ArithmeticError("0^0 is undefined")
    if a == 1 or b == 0:
        return 1
    if a == 0:
        return 0
    if b == 1:
        return a
    return a * r_power(a, b - 1)


if __name__ == '__main__':
    l = [2, 4, 8, 16, 32, 64]
    rl = [1, 2, [3, 4], [5, 6, [7, 8, [9, 10]]]]
    # print(list_r_sum(rl))
    # print(digit_sum(345))
    # print(digit_sum(45))
    # print(sum_n_minus_even(6))
    # print(sum_n_minus_even(10))
    # print(harmonic_sum(7))
    # print(harmonic_sum(4))
    # print(r_power(3, 6))
    # print(r_power(2, 16))
