from math import *
from cmath import *


def solve(a, b, c):
    """
    Solves a quadratic equation, a*x²+b*x+c=0
    :param a: first argument
    :param b: second argument
    :param c: third arguments
    :return: list of solutions, real and complex
    """
    d = b ** 2 - 4 * a * c
    if d < 0:
        # complex solutions
        # real = -b/2a
        # complex = +-sqrt(-1 * -d)/2a
        return [complex(-b / (2 * a), sqrt(d) / (2 * a)), complex(-b / (2 * a), -sqrt(d) / (2 * a))]
    elif d == 0:
        # single solution
        return [-b / (2 * a)]
    else:
        # 2 solutions
        return [(-b + sqrt(d)) / (2 * a), (-b - sqrt(d)) / (2 * a)]
