class ChiffreLettre:
    ZERO = "zéro"

    def __init__(self, nombre):
        self.solve = self.mdcl(nombre)

    def mdcl(self, nombre: int, zero=ZERO):
        nombre = f"{nombre:012d}"
        md = int(nombre[:-9])
        if md == 0:
            return self.micl(int(nombre), zero)
        mi = self.micl(int(nombre[-9:]), "")
        left = self.mdcl(md, "")
        plural = "" if md == 1 else 's'
        return f"{left} milliard{plural} {mi}".strip()

    def micl(self, nombre: int, zero=ZERO):
        nombre = f"{nombre:09d}"
        mi = int(nombre[:3])
        if mi == 0:
            return self.mcl(int(nombre), zero)
        mm = self.mcl(int(nombre[3:]), "")
        cmi = self.ccl(mi, "", True)
        plural = "" if mi == 1 else 's'
        return f"{cmi} million{plural} {mm}".strip()

    def mcl(self, nombre: int, zero=ZERO):
        nombre = f"{nombre:06d}"
        m = int(nombre[:3])
        # special case without thousands
        if m == 0:
            return self.ccl(int(nombre), zero)
        cu = self.ccl(int(nombre[3:]), "")
        cm = "" if m == 1 else self.ccl(m, "", True)  # special case when 1000
        return f"{cm} mille {cu}".strip()

    def ccl(self, nombre: int, zero=ZERO, override_plural=False):
        centaines = ["", "", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf"]
        nombre = f"{nombre:03d}"
        c = int(nombre[0])
        # special case without cents
        if c == 0:
            return self.ducl(int(nombre), zero, override_plural)
        du = self.ducl(int(nombre[1:]), "", override_plural)
        centaine = f"{centaines[c]} cent"
        # plural
        plural = ""
        if not du and c != 1 and not override_plural:
            plural = 's'
        return f"{centaine}{plural} {du}".strip()

    def ducl(self, nombre: int, zero=ZERO, override_plural=False):
        chiffres = ["", "un", "deux", "trois", "quatre", "cinq", "six", "sept", "huit", "neuf"]
        tens = [zero, "dix", "vingt", "trente", "quarante", "cinquante", "soixante", "soixante", "quatre-vingt",
                "quatre-vingt"]
        first_ten = ["dix", "onze", "douze", "treize", "quatorze", "quinze", "seize", "dix-sept", "dix-huit",
                     "dix-neuf"]
        # special cases
        unique_tens = {0, 1}
        dix_tens = {1, 7, 9}
        vigesimal = {8, 9}
        # number has to be at least 2 digits
        nombre = f"{nombre:02d}"
        d = int(nombre[0])
        u = int(nombre[1])
        # special case zero
        if d == 0 and u == 0:
            return zero

        unite = first_ten[u] if d in dix_tens else chiffres[u]
        # plural, 80 avec un s seulement s'il n'y a rien derriere (quatre-vingt_ millions)
        plural = ""
        if d == 8 and u == 0 and not override_plural:
            plural = 's'
        hyphen = "" if u == 0 and d not in dix_tens else '-'
        et = "" if u != 1 or d in vigesimal else "et-"
        dizaine = "" if d in unique_tens else f"{tens[d]}{plural}{hyphen}{et}"

        return f"{dizaine}{unite}"

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return str(self.solve)
