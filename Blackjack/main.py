from random import Random


class Player:
    def __init__(self, name, cards=None):
        if cards is None:
            cards = []
        self.name = name
        self.cards = cards
        self.drew = False

    def points(self):
        return sum(card.number for card in self.cards)

    def draw(self, stack):
        self.cards.append(stack.pop(0))
        self.drew = True
        return stack

    def __repr__(self):
        return f"<{self.name}__{self.cards}>"


class Card:
    HEART = "heart"
    DIAMOND = "diamond"
    SPADE = "spade"
    CLUB = "club"

    KING = "king"
    QUEEN = "queen"
    JACK = "jack"
    ACE = "ace"
    NOTHING = "nothing"

    def __init__(self, number, color, fig):
        self.number = number
        self.color = color
        self.fig = fig

    def __repr__(self):
        txt_fig = self.fig if self.fig != self.NOTHING else ""
        return f"<{self.number}_{self.color}_{txt_fig}>"
        # return f"<{self.number}_{self.color}_{self.fig}>"


def play(player, deck, condition):
    if condition:
        player.draw(deck)
        print(f"{player.name} draws {player.cards[-1]}")
        player.drew = True
        return player.points() > 21
    else:
        player.drew = False
    return False


if __name__ == "__main__":
    figures = [Card(11, color, fig) for color in [Card.HEART, Card.DIAMOND, Card.SPADE, Card.CLUB] for fig in
               [Card.KING, Card.QUEEN, Card.JACK, Card.ACE]]
    normal_cards = [Card(number, color, Card.NOTHING) for color in [Card.HEART, Card.DIAMOND, Card.SPADE, Card.CLUB] for
                    number in range(2, 11)]
    deck = figures + normal_cards
    pile = []
    Random().shuffle(deck)
    print(deck)

    first = Random().choice([True, False])  # True = player, False = computer
    lost = False
    win = False
    human = Player("player")
    computer = Player("computer")

    print("You play first" if first else "Computer plays first")
    while not win and not lost:
        if first:
            if play(human, deck, input("Do you draw? Y/N ") == 'Y'):
                print("You lost")
                win = True
            elif play(computer, deck, computer.points() < 17):
                print("You won")
                lost = True
        else:
            if play(computer, deck, computer.points() < 17):
                print("You won")
                win = True
            elif play(human, deck, input("Do you draw? Y/N ") == 'Y'):
                print("You lost")
                lost = True
        if computer.points() == 21:
            print("You lost")
            lost = True
            break
        if human.points() == 21:
            print("You won")
            win = True
            break
        if not human.drew and not computer.drew:
            if human.points() == computer.points():
                print("Tie")
                break
            most_points = max(human, computer, key=lambda k: k.points())
            print(f"{most_points.name} has won")
            if most_points == human:
                win = True
            if most_points == computer:
                lost = True
