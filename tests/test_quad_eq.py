from unittest import TestCase
from quad_eq import solve
from math import *
from cmath import *


class Test(TestCase):
    def d(self, a, b, c):
        return b ** 2 - 4 * a * c

    def test_solve_complex(self):
        a, b, c = 1, 2, 2
        self.assertEqual(solve(a, b, c),
                         [complex(-b / (2 * a), sqrt(self.d(a, b, c)) / (2 * a)),
                          complex(-b / (2 * a), -sqrt(self.d(a, b, c)) / (2 * a))])

    def test_solve_real(self):
        a, b, c = 4, 26, 12
        self.assertEqual(solve(a, b, c), [(-b + 22) / (2 * a), (-b - 22) / (2 * a)])

    def test_solve_one(self):
        a, b, c = 1, 2, 1
        self.assertEqual(solve(a, b, c), [-1])
