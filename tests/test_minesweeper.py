from unittest import TestCase
from Minesweeper.Minesweeper import Minesweeper


class MyTestCase(TestCase):
    def test_tap(self):
        board = [[1, '*', 1, 1, 1, 2, 1, 1, 0, 0],
                 [1, 1, 1, 0, 0, 1, '*', 1, 0, 0],
                 [0, 0, 0, 0, 0, 2, 2, 2, 0, 0],
                 [0, 0, 0, 0, 0, 1, '*', 1, 0, 0],
                 [1, 0, 0, 0, 0, 1, 1, 1, 1, 1],
                 [2, 0, 0, 0, 0, 0, 0, 0, 2, '*'],
                 [2, 0, 1, 1, 1, 0, 1, 1, 3, '*'],
                 [1, 0, 1, '*', 1, 0, 2, '*', 3, 1],
                 [0, 0, 1, 2, 2, 1, 2, '*', 2, 0],
                 [0, 0, 0, 1, '*', 1, 1, 1, 1, 0]]
        board = [[str(elem) for elem in row] for row in board]
        grid1 = Minesweeper(10, 10, board=board)
        print(grid1.tap(0, 2))
