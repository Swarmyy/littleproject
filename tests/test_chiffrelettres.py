from unittest import TestCase
from chiffrelettres import ChiffreLettre


class Test(TestCase):
    def test_ChiffreLettre(self):
        self.assertEqual("huit cents", ChiffreLettre(800).__str__())
        self.assertEqual("cent quatre-vingts", ChiffreLettre(180).__str__())
        self.assertEqual("cent", ChiffreLettre(100).__str__())
        self.assertEqual(ChiffreLettre.ZERO, ChiffreLettre(0).__str__())
        self.assertEqual("huit cent quatre-vingts", ChiffreLettre(880).__str__())
        self.assertEqual("dix", ChiffreLettre(10).__str__())
        self.assertEqual("quatre-vingt mille cent quatre-vingts", ChiffreLettre(80180).__str__())
        self.assertEqual("quatre-vingt mille", ChiffreLettre(80000).__str__())
        self.assertEqual("huit cent mille", ChiffreLettre(800000).__str__())
        self.assertEqual("quatre cents", ChiffreLettre(400).__str__())
        self.assertEqual("mille", ChiffreLettre(1000).__str__())
        self.assertEqual("quatre-vingts", ChiffreLettre(80).__str__())
        self.assertEqual("quatre-vingt-dix-neuf", ChiffreLettre(99).__str__())
        self.assertEqual("six cent trente-cinq mille huit cent trente-sept", ChiffreLettre(635837).__str__())
        self.assertEqual("un million", ChiffreLettre(1000000).__str__())
        self.assertEqual("deux millions", ChiffreLettre(2000000).__str__())
        self.assertEqual("quarante-cinq millions six cent dix-huit mille neuf cent soixante-treize",
                         f"{ChiffreLettre(45618973)}")
        self.assertEqual(
            "neuf cent quatre-vingt-dix-neuf millions neuf cent quatre-vingt-dix mille neuf cent quatre-vingt-dix-neuf",
            ChiffreLettre(999990999).__str__())
        self.assertEqual(
            "neuf cent quatre-vingt-dix-neuf millions neuf cent quatre-vingt-dix-neuf mille neuf cent "
            "quatre-vingt-dix-neuf",
            ChiffreLettre(999999999).__str__())
        self.assertEqual("un milliard", f"{ChiffreLettre(1000000000)}")
        self.assertEqual("deux milliards", f"{ChiffreLettre(2000000000)}")
        self.assertEqual("trois milliards cinq", f"{ChiffreLettre(3000000005)}")
        self.assertEqual("trois mille milliards", f"{ChiffreLettre(3000000000000)}")
