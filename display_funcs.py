def filter_opt(opt: dict, t):
    return {k: v for k, v in opt.items() if isinstance(v, t)}


if __name__ == "__main__":
    options = {
        "x": {"d": "d.x"},
        "y": {"d": "d.y"},
        "z": {"d": "d.z"},
        "yLabel": "y",
        "height": 500,
        "color": "steelblue",
    }

    print(len(filter_opt(options, dict)))
    print(callable(filter_opt))
