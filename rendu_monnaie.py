class ChangeT:
    ALL = 0
    GT = 1
    NOT_ENOUGH = 2


def get_change(price, values, caisse: dict):
    state = ChangeT.ALL
    change = {}
    values.sort(reverse=True)
    currencies = list(caisse.keys())
    currencies.sort(reverse=True)
    """for v in values:
        n = price // v
        bill[v] = n
        price -= n * v"""
    for ccy in currencies:
        amount=caisse[ccy]
        n_bill = price // ccy
        if n_bill > amount:
            n_bill = amount
        change[ccy] = n_bill
        caisse[ccy] -= n_bill
        price -= n_bill * ccy

    if price > 0:
        state = ChangeT.GT
        leftovers = [ccy for ccy, amount in caisse.items() if amount > 0]
        leftovers.sort()
        try:
            change[leftovers[0]] += 1
            price -= leftovers[0]
        except IndexError:
            pass

    if price > 0:
        state = ChangeT.NOT_ENOUGH
    return change, state


def to_mornilles(g, m):
    return g * 17 + m


def to_noises(g, m, n):
    return to_mornilles(g, m) * 29 + n


def main():
    values_e = [1, 2, 5, 10, 20, 50, 100, 200, 500]
    values_w = [1, 29, 29 * 17]
    values = values_w

    caisse = {
        1: 10 ** 6,
        29: 10 ** 6,
        29 * 17: 10 ** 6
    }

    prices_e = [0, 60, 63, 231, 899]
    prices_w = [0, 654, to_noises(0, 23, 78), to_noises(2, 11, 9), to_noises(7, 531, 451)]
    prices = prices_w

    s_to_str = {
        ChangeT.ALL: "Le compte est bon",
        ChangeT.GT: "Le rendu est plus grand que la somme demandée",
        ChangeT.NOT_ENOUGH: "Il n'y a pas assez de billets dans la caisse"
    }

    for p in prices:
        print(f"{p}:")

        billets, state = get_change(p, values, caisse.copy())
        print(s_to_str[state])
        for k, v in billets.items():
            print(f"Il y a {v} billets de {k} à rendre")
        print('')


if __name__ == '__main__':
    main()
