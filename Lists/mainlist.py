from ListStart import ListStart


def index(list, x, *args):
    return list.index(x, *args)


if __name__ == "__main__":
    firstlist = ListStart([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 1)
    b = [1, 2, 3, 4, 5, 6, 7, 8]
    print(index(b, 3, 0, 9))
