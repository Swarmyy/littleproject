class ListStart:
    def __init__(self, list: list, start_index: int = 0):
        self.list = list.copy()
        self.start_index = start_index

    def append(self, x):
        self.list.append(x)

    def extend(self, iterable):
        self.list += iterable

    def insert(self, i, x):
        self.__setitem__(i, x)

    def remove(self, x):
        self.list.remove(x)

    def pop(self, i):
        return self.list.pop(i + self.start_index)

    def clear(self):
        self.list.clear()

    def index(self, x, *args):
        return self.list.index(x, *args)

    def count(self, x):
        return self.list.count(x)

    def sort(self, *args, **kwargs):
        self.list.sort(*args, **kwargs)

    def reverse(self):
        self.list.reverse()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return self.list.copy()

    def __getitem__(self, index):
        return self.list.__getitem__(index - self.start_index)

    def __setitem__(self, key, value):
        self.list.__setitem__(key - self.start_index, value)

    def __add__(self, other):
        return ListStart(self.list + other, self.start_index)

    def __eq__(self, other):
        return self.list == other.list
