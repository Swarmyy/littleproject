from abc import ABC
from ctypes import *
from typing import Iterable, Sized


class ListStart:
    def __init__(self, iterable=None, start_index: int = 0, circular: bool = False):
        self.next = None
        if iterable is not None:
            self.data = iterable[0]
            if len(iterable) > 1:
                self.next = ListStart(iterable[1:], start_index)
        self.start_index = start_index
        self.circular = circular

    def append(self, value):
        if self.data is None:
            self.data = value
        elif self.next is None:
            self.next = ListStart(value)
        else:
            self.next.append(value)

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return ListStart(self, self.start_index, self.circular)

    def __getitem__(self, index):
        if index == 0:
            return self.data
        return self.next[index - 1]

    def __setitem__(self, key, value):
        if key == 0:
            self.data = value
        self.next[key - 1] = value

    def __add__(self, other):
        concatenation = self.copy()
        for m in other:
            concatenation.append(m)
        return concatenation

    def __eq__(self, other):
        pass

    def __len__(self):
        pass
