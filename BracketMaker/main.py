from Bracket import Bracket
from Match import Match
from random import Random


def random_winner_match(match, bo=3):
    winner = Random().choice([match.player1, match.player2])
    winner_score = bo // 2 + 1
    loser_score = Random().randint(0, bo // 2)
    scorep1 = winner_score if winner == match.player1 else loser_score
    scorep2 = loser_score if winner == match.player1 else winner_score
    match.result(scorep1, scorep2)


def random_winner_bracket(bracket):
    max_rounds = 1
    while bracket.round(max_rounds)[0].win != Match.DEFWIN:
        max_rounds += 1
    for n_round in range(1, max_rounds):
        if n_round < max_rounds:
            this_round = bracket.round(n_round)
            for round in this_round:
                random_winner_match(round)
            continue
    return bracket


def display_bracket(bracket):
    """
    Single elimination brackets
    :param bracket:
    :return:
    """
    max_rounds = 1
    while bracket.round(max_rounds)[0].win != Match.DEFWIN:
        max_rounds += 1
    text = ""
    text += display_rounds(bracket, 1, max_rounds)
    return text


def slug_one_round(round, pre="", suf="\n"):
    """
    Displays properly a round, without the lose/win match display
    :param round:
    :return: str
    """
    text = ""
    for m in round:
        text += f"{pre}{m}{suf}"
    return text


def display_rounds(bracket, n_round, max_rounds):
    tabs = '\t' * (n_round - 1)
    if n_round == max_rounds:
        return slug_one_round(bracket.round(n_round), tabs)
    text = ""
    this_round = bracket.round(n_round)
    for nm in range(0, len(this_round), 2):
        text += f"{tabs}{this_round[nm]}\n"
        text += display_rounds(bracket, n_round + 1, max_rounds)
        text += f"{tabs}{this_round[nm + 1]}\n"
    return text


if __name__ == '__main__':
    bracket1 = Bracket(
        [f"Player {i}" for i in range(1, 17)])
    bracket1.generate()
    random_winner_bracket(bracket1)
    print(display_bracket(bracket1))
