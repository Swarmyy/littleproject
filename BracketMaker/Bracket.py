from random import Random
from Match import Match


class Bracket:
    SINGLE_BRACKET = "single bracket1"

    def __init__(self, players, flags=None):
        if flags is None:
            flags = [Bracket.SINGLE_BRACKET]
            # I have to note that not recognizing Bracket in the parameters but in the
            # init is kinda disturbing me
        self.players = players
        self.flags = flags
        self.first_matches = []
        self.total_matches = []

    def generate(self):
        Random().shuffle(self.players)
        self.first_matches = [Match(self.players[i], self.players[i + 1], None, Match.DEFLOSE) for i in
                              range(0, len(self.players), 2)]
        self.create_next_matches(self.first_matches)

    def create_next_matches(self, matches):
        if len(matches) <= 1:
            matches[0].win = Match.DEFWIN
            matches[0].lose = Match.DEFLOSE
        elif len(matches) == 2:
            final = Match(None, None, Match.DEFWIN, Match.DEFLOSE)
            matches[0].win = final
            matches[1].win = final
            matches[0].lose = matches[1].lose = Match.DEFLOSE
            self.total_matches.append(final)
        else:
            next_matches = []
            for i in range(0, len(matches), 2):
                win_match = Match(None, None, None, Match.DEFLOSE)
                matches[i].win = win_match
                matches[i + 1].win = win_match
                next_matches.append(win_match)
            self.create_next_matches(next_matches)
        self.total_matches += matches

    def round(self, n):
        if n == 0:
            return self.first_matches
        if n == 1:
            return self.first_matches
        return list(dict.fromkeys([m.win for m in self.round(n - 1)]))

    def __str__(self):
        text = "["
        for m in self.total_matches:
            text += f"{str(m)},"
        return text[:-1] + "]"
