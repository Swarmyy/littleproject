class Match:
    DEFWIN = "W"
    DEFLOSE = "L"

    def __init__(self, player1, player2, win, lose, scorep1=0, scorep2=0):
        self.player1 = player1
        self.player2 = player2
        self.win = win
        self.lose = lose
        self.scorep1 = scorep1
        self.scorep2 = scorep2

    def add_player(self, player):
        if self.player1 is None:
            self.player1 = player
        elif self.player2 is None:
            self.player2 = player
        else:
            raise Exception("Match already has two players")

    def set_winner_loser(self, winner, loser):
        if self.win == Match.DEFWIN:
            self.lose.add_player(loser)
            return Match.DEFWIN
        if self.lose == Match.DEFLOSE:
            self.win.add_player(winner)
            return Match.DEFLOSE
        self.win.add_player(winner)
        self.lose.add_player(loser)
        return False

    def result(self, score_p1, score_p2):
        self.scorep1 = score_p1
        self.scorep2 = score_p2
        if score_p1 > score_p2:
            return self.set_winner_loser(self.player1, self.player2)
        if score_p2 > score_p1:
            return self.set_winner_loser(self.player2, self.player1)
        if score_p1 == score_p2:
            raise Exception("Tie game")

    def __str__(self):
        return f"{self.player1} {self.scorep1} - {self.scorep2} {self.player2}"

    def __repr__(self):
        return f"<{self}; {self.win} {self.lose}>"
