from random import randint


class Game:

    def __init__(self, players=None, stack=None):
        if players is None:
            players = []
        if stack is None:
            stack = []
        self.players = players
        self.drawed = []
        self.stack = stack

        self.nextp = randint(0, len(players))
        self.reverse = False
        self.skip = 0

    def next(self):
        self.nextp += (-1 if self.reverse else 1) * (1 + self.skip)
        self.skip = 0
