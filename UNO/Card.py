class Card:

    def __init__(self, number, color, action):
        """
        :param number:
        :param color:
        :param action: (game, target, caster, **kwargs)
        """
        self.number = number
        self.color = color
        self.action = action

    def __repr__(self):
        return f"<{self.number}_{self.color}_{self.action}>"
