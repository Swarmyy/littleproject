from random import Random
from Game import Game
from Player import Player
from Card import Card
from templates import YELLOW, GREEN, RED, BLUE, COLOR

ALL_COLORS = YELLOW, GREEN, RED, BLUE

if __name__ == "__main__":
    stack = [Card(Random().randint(0, 9), Random().choice(ALL_COLORS), Random().choice(COLOR)) for _ in
             range(0, 80)]
    players = [Player(f"Player {i}") for i in range(1, 5)]
    game = Game(players, stack)
    print(game)
