YELLOW = "yellow"
BLUE = "blue"
RED = "red"
GREEN = "green"


def plus_four_act(game, target, caster, **kwargs):
    """
    Draws four cards to the next player(target) and changes color
    :param game:
    :param target:
    :param caster:
    :param kwargs:
    :return:
    """
    target.draw()
    target.draw()
    target.draw()
    target.draw()
    choose_color_act(game, target, caster, **kwargs)


def choose_color_act(game, target, caster, **kwargs):
    """

    :param game:
    :param target:
    :param caster:
    :param kwargs: needs a color parameter to work
    :return:
    """
    game.drawed[-1].color = kwargs["color"]


def plus_2_act(game, target, caster, **kwargs):
    target.draw()
    target.draw()


def skip_act(game, target, caster, **kwargs):
    game.skip = 1


def reverse_act(game, target, caster, **kwargs):
    game.reverse = not game.reverse


COLOR = [plus_2_act, reverse_act, skip_act]
