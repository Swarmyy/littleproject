class Player:

    def __init__(self, name, cards=None):
        if cards is None:
            cards = []
        self.name = name
        self.cards = cards

    def play(self, card):
        self.cards.remove(card)

    def draw(self, stack):
        self.cards.append(stack[0])
        return stack[1:]

    def __repr__(self):
        return f"<{self.name}__{self.cards}>"
