from random import Random


class Minesweeper:
    FLAG = "F"
    UNKNOWN = "?"
    EMPTY = "_"
    MINE = "*"

    def __init__(self, n_rows: int, n_columns: int, n_mines: int = 0, luck: float = 0.0, board=None,
                 display_board=None):
        self.n_rows = n_rows
        self.n_columns = n_columns
        self.n_mines = n_mines
        self.luck = luck
        # initialize the board
        if display_board is None:
            self.display_board = [[self.UNKNOWN for _ in range(n_rows)] for _ in range(n_columns)]
        else:
            self.display_board = display_board
            return
        if board is None:
            self.board = [[self.EMPTY for _ in range(n_rows)] for _ in range(n_columns)]
        else:
            self.board = board
            return
        # manage n_mines and luck
        # parentheses are mandatory because the xor has priority
        if (n_mines == 0) ^ (luck == 0):  # set either the mines or luck depending on which is set
            if n_mines == 0 and luck != 0:
                self.n_mines = self.get_n_mines(luck)
            if n_mines != 0 and luck == 0:
                self.luck = self.get_luck(n_mines)
            self.generate_board()
        # else do nothing

    def generate_mines(self):
        left_mines = self.n_mines
        for x in range(self.n_rows):
            for y in range(self.n_columns):
                # parentheses are mandatory because the and has priority
                if (Random().random() < self.luck) & (left_mines > 0):
                    self.board[x][y] = self.MINE
                    left_mines -= 1

    def generate_board(self):
        self.generate_mines()
        for x in range(self.n_rows):
            for y in range(self.n_columns):
                if self.board[x][y] != self.MINE:
                    self.board[x][y] = str(self.count_mines(x, y))

    def count_mines(self, x: int, y: int):
        count = 0
        for h in range(3):
            for v in range(3):
                if h == 1 and v == 1:
                    continue
                h_pos = x + h - 1
                v_pos = y + v - 1
                if 0 <= h_pos < self.n_columns and 0 <= v_pos < self.n_rows:
                    if self.board[v_pos][h_pos] == self.MINE:
                        count += 1
        return count

    def get_n_mines(self, luck: float):
        return self.n_rows * self.n_columns * luck

    def get_luck(self, n_mines: int):
        return n_mines / (self.n_rows * self.n_columns)

    def display_mines(self):
        for x in range(self.n_rows):
            for y in range(self.n_columns):
                if self.board[x][y] == self.MINE:
                    self.display_board[x][y] = self.MINE

    def reveal(self, x: int, y: int):
        """Reveal the cell at x, y + try catch if out of bounds"""
        try:
            self.display_board[x][y] = self.board[x][y]
        except IndexError:
            pass

    def reveal_shift(self, coords):
        for x, y in coords:
            self.reveal(x, y)

    def reveal_up(self, x, y):
        for v in range(3):
            self.reveal(x - 1, y + v - 1)

    def reveal_down(self, x, y):
        for v in range(3):
            self.reveal(x + 1, y + v - 1)

    def reveal_right(self, x, y):
        for h in range(3):
            self.reveal(x + h - 1, y + 1)

    def reveal_left(self, x, y):
        for h in range(3):
            self.reveal(x + h - 1, y - 1)

    def reveal_up_left_corner(self, x, y):
        coords = [(0, -1), (-1, -1), (-1, 0)]
        self.reveal_shift([(x + h, y + v) for h, v in coords])

    def reveal_up_right_corner(self, x, y):
        coords = [(-1, 0), (-1, 1), (0, 1)]
        self.reveal_shift([(x + h, y + v) for h, v in coords])

    def reveal_down_left_corner(self, x, y):
        coords = [(0, -1), (1, -1), (1, 0)]
        self.reveal_shift([(x + h, y + v) for h, v in coords])

    def reveal_down_right_corner(self, x, y):
        coords = [(1, 0), (1, 1), (0, 1)]
        self.reveal_shift([(x + h, y + v) for h, v in coords])

    def reveal_area(self, x, y):
        self.display_board[y][x] = self.EMPTY
        for v in range(3):
            for h in range(3):
                if h == 1 and v == 1:
                    continue
                h_pos = x + h - 1
                v_pos = y + v - 1
                if 0 <= h_pos < self.n_columns and 0 <= v_pos < self.n_rows:
                    if self.board[v_pos][h_pos] == self.EMPTY or self.board[v_pos][h_pos] == '0':
                        self.display_board[v_pos][h_pos] = self.EMPTY
                        self.reveal_area(h_pos, v_pos)
                        continue
                    if self.board[v_pos][h_pos].isdigit():
                        self.display_board[v_pos][h_pos] = self.board[v_pos][h_pos]
                        continue

    def tap(self, x, y):
        """
        :param x: x position in the grid
        :param y: y position in the grid
        :return: False if loose, self if otherwise
        """
        if self.board[y][x] == self.MINE:
            self.display_mines()
            return False
        elif self.board[y][x] == self.EMPTY or self.board[y][x] == '0':
            self.reveal_area(x, y)
        elif self.board[y][x].isdigit():
            self.display_board[y][x] = self.board[y][x]
        # default return
        return self

    def flags(self):
        count = 0
        for x in range(self.n_rows):
            for y in range(self.n_columns):
                if self.display_board[x][y] == self.FLAG:
                    count += 1
        return count

    def is_won(self):
        for x in range(self.n_rows):
            for y in range(self.n_columns):
                if self.board[x][y] == self.MINE and self.display_board[x][y] != self.FLAG:
                    return False
        return True

    def slug_board(self, board=None):
        if board is None:
            board = self.display_board
        return "\n".join([" ".join(row) for row in board])

    def __str__(self):
        return self.slug_board(self.display_board)
