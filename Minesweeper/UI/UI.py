from tkinter import Entry, END


class UI:
    def __init__(self, frame, table):
        self.frame = frame
        # code for creating table
        for i in range(len(table)):
            for j in range(len(table[i])):
                self.e = Entry(frame, width=16, fg='blue',
                               font=('Arial', 10, 'bold'))

                self.e.grid(row=i, column=j)
                self.e.insert(END, table[i][j])
